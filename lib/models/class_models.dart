/**
 * Created by Bayu Nugroho
 * Copyright (c) 2020 . All rights reserved.
 */

class ModelMakanan {
  String name;
  String img;
  int qty;
  int price;

  ModelMakanan({this.name, this.img, this.qty, this.price});
}