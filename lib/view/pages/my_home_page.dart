import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:point_of_sales/models/class_models.dart';
import 'package:point_of_sales/utils/size_config.dart';

/**
 * Created by Bayu Nugroho
 * Copyright (c) 2020 . All rights reserved.
 */

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  List<ModelMakanan> listMakanan = [];
  int totalQty = 0;
  int totalharga = 0;


  @override
  void initState() {
    setData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    //inisialisasi class SizeCongfig
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(title: Text('Point Of Sales'),
      actions: [
        Padding(
          padding: const EdgeInsets.only(right: 20,top: 20),
          child: Text('Rp. $totalharga'),
        )
      ],
      ),
      body: Container(
        child: Column(
          children: [
            buildMenuMakanan()
          ],
        ),
      ),
    );
  }

  Widget buildMenuMakanan(){
    return SizedBox(
      height: SizeConfig.screenHight / 1.2,
      child: FutureBuilder(
        future: getMakanan(),
        builder: (context,snapshot){
          if(snapshot.hasData){
            return ListView.builder(
                itemCount: snapshot.data.length,
                scrollDirection: Axis.vertical,
                physics: ClampingScrollPhysics(),
                itemBuilder: (context, index) {
                  return Card(
                    child: Column(
                      children: [
                        Image.asset(
                            listMakanan[index].img,
                        ),
                        Text(listMakanan[index].name ,style: TextStyle(color: Colors.pink,fontSize: SizeConfig.blockVertical * 3),),
                        Text("Rp. ${listMakanan[index].price}" ,style: TextStyle(color: Colors.pink,fontSize: SizeConfig.blockVertical * 2),),
                        Row(
                          children: [
                            IconButton(
                              onPressed: () {
                                  setState(() {
                                    totalQty ++;
                                    totalharga += listMakanan[index].price;
                                  });
                              }, icon: Icon(Icons.add),
                            ),
                            Text('${totalQty}'),
                            IconButton(
                              onPressed: () {
                                if(totalQty > 0){
                                  setState(() {
                                    totalQty --;
                                    totalharga -= listMakanan[index].price;
                                  });
                                }
                              }, icon: Icon(Icons.minimize),
                            ),
                          ],
                        ),
                      ],
                    ),
                  );
                });
          }else{
            return Container();
          }
        },
      ),
    );
  }
  Future<List<ModelMakanan>> getMakanan() async {
    return new Future.delayed(new Duration(seconds: 3), () {
      return listMakanan;
    });
  }

  void setData() {
    setState(() {
      listMakanan.add(new ModelMakanan(
          name: "soto",
          price: 15000,
          img: "assets/img/makan.jpg",
          qty:10
      ));

      listMakanan.add(new ModelMakanan(
          name: "soto",
          price: 15000,
          img: "assets/img/makan.jpg",
          qty:10
      ));
    });
  }

}
